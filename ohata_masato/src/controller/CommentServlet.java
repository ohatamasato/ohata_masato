package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;
/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> commentMessages = new ArrayList<String>();

        if (isValid(request, commentMessages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment ();
            comment.setId(user.getId());
            comment.setText(request.getParameter("comment"));
            comment.setContributions_id(Integer.parseInt(request.getParameter("contributions_id")));

            new CommentService ().CommentsRegister(comment);
            request.setAttribute("comment", comment);
            response.sendRedirect("./");

        } else {
        	Comment commentview = new Comment();
            commentview.setText(request.getParameter("comment"));
            commentview.setContributions_id(Integer.parseInt(request.getParameter("contributions_id")));


            session.setAttribute("commentview",commentview);
            session.setAttribute("commenterrorMessages",commentMessages);

            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> commenterrorMessages) {

        String commenterrorMessage = request.getParameter("comment");

        if (StringUtils.isEmpty(commenterrorMessage) == true) {
            commenterrorMessages.add("コメントを入力してください");
        }
        if(500 < commenterrorMessage.length()){
        	commenterrorMessages.add("500文字以下で入力してください");
        }
        if (commenterrorMessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

