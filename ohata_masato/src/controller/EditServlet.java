package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.EditService;
import service.UserService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/Edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

 @Override
    protected void doGet(HttpServletRequest request,
    HttpServletResponse response) throws ServletException, IOException {

	 EditService editService = new EditService();
     	//IDがnullかどうか
		if(request.getParameter("id")==null){
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", "IDが不正です。");
			response.sendRedirect("Management");
			return;
		}

		//urlIDが数字か判断
		String idCheck = request.getParameter("id");
		if( !idCheck.matches("[0-9]+")){
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", "IDが不正です。");
			response.sendRedirect("Management");
			return;
		}

		int id = Integer.parseInt(request.getParameter("id"));
		boolean idc = new UserService().getId(id);

		//urlのidが存在するか判断
		if(idc == true){
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", "IDが不正です。");
			response.sendRedirect("Management");
			return;

		} else {
			HttpSession session = ((HttpServletRequest)request).getSession();

			User user = new User();
			user.setId(Integer.parseInt(request.getParameter("id")));

			User loginUser = (User) session.getAttribute("loginUser");
			User editUser = editService.getUserEdit(user);
			List<Branch> branchName = editService.getBranchName();
			List<Department> departmentName = editService.getDepartmentName();

			request.setAttribute("loginUser", loginUser);
	        request.setAttribute("editUser", editUser);
	        request.setAttribute("branchName", branchName);
	        request.setAttribute("departmentName", departmentName);

			request.getRequestDispatcher("/EditUser.jsp").forward(request, response);
		}
 	}

 //post文
    @Override
    protected void doPost(HttpServletRequest request,
    HttpServletResponse response) throws ServletException, IOException {

        List<String> editmessages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if (isValid(request, editmessages) == true) {
        	if(StringUtils.isEmpty(request.getParameter("password"))){
        		User editUser = new User();
            	editUser.setId(Integer.parseInt(request.getParameter("id")));
                editUser.setName(request.getParameter("name"));
                editUser.setLogin_id(request.getParameter("login_id"));
                editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
                editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
                request.setAttribute("editUser", editUser);
                new UserService().reUpdate(editUser);
                new EditService().reEdit(editUser);
                response.sendRedirect("Management");
        	}else{
        		User editUser = new User();
            	editUser.setId(Integer.parseInt(request.getParameter("id")));
                editUser.setName(request.getParameter("name"));
                editUser.setPassword(request.getParameter("password"));
                editUser.setLogin_id(request.getParameter("login_id"));
                editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
                editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
                request.setAttribute("editUser", editUser);
                new EditService().edit(editUser);
                //new UserService().update(editUser);
                response.sendRedirect("Management");
        	}
        } else {
            session.setAttribute("editmessages", editmessages);

            EditService editService = new EditService();
    		List<Branch> branchName = editService.getBranchName();
    		List<Department> departmentName = editService.getDepartmentName();

            User reEditUser = new User();
        	reEditUser.setId(Integer.parseInt(request.getParameter("id")));
        	reEditUser.setName(request.getParameter("name"));
        	reEditUser.setPassword(request.getParameter("password"));
        	reEditUser.setLogin_id(request.getParameter("login_id"));
        	reEditUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        	reEditUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

        	request.setAttribute("editUser", reEditUser);
        	request.setAttribute("branchName", branchName);
	        request.setAttribute("departmentName", departmentName);

            request.getRequestDispatcher("/EditUser.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> editmessages) {

        String Name = request.getParameter("name");
        String Login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String passwordconfirm = request.getParameter("passwordconfirm");
        String branch_id =request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");
        int Id=Integer.parseInt(request.getParameter("id"));
        boolean idCheck = new UserService().getIdCheck(Id,Login_id);

        if (StringUtils.isEmpty(Name) == true) {
            editmessages.add("名前を入力してください");
        }
        if (10<Name.length()) {
            editmessages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(Login_id) == true) {
            editmessages.add("ログインIDを確認してください");
        }
        if(idCheck==false){
			editmessages.add("ログインIDが重複しています");
		}
        if(StringUtils.isBlank(password) == false &&!password.matches("[!-~]{6,20}")){
        	editmessages.add("パスワードは記号を含むすべての半角英数字で6から20文字以下で入力してください。");
        }
        if (!password.equals(passwordconfirm)){
            editmessages.add("パスワードと確認用パスワードが一致しません");
        }
        if(branch_id.matches("[1]") && department_id.matches("[3]")){
        	editmessages.add("支店名と部署・役職の組み合わせが不正です");
        }
        if(!branch_id.matches("[1]") && department_id.matches("[1-2]")){
        	editmessages.add("支店名と部署・役職の組み合わせが不正です");
        }
        if (editmessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

   /* public boolean isNumber(String num){
    	try{
    		Integer.parseInt(num);
    		return true;
    	}catch(NumberFormatException e){
    		return false;
    	}
    }*/
}
