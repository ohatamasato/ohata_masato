package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.ManagementService;

/**
 * Servlet implementation class UserManagementServlet
 */
@WebServlet(urlPatterns = {"/Management"})
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session=((HttpServletRequest)request).getSession();

        User loginUser =(User) session.getAttribute("loginUser");
        request.setAttribute("loginUser",loginUser);

        List<User> UserList= new ManagementService().getdata();
    	request.setAttribute("getdata",UserList);

        request.getRequestDispatcher("UserMng.jsp").forward(request, response);
    }

}