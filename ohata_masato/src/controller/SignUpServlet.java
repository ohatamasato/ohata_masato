package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.EditService;
import service.UserService;

	@WebServlet(urlPatterns = { "/signup" })
	public class SignUpServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		EditService editService = new EditService();
		@Override
		protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
			EditService editService = new EditService();

			List<Branch> branchName = editService.getBranchName();
			List<Department> departmentName = editService.getDepartmentName();
	        request.setAttribute("branchName", branchName);
	        request.setAttribute("departmentName", departmentName);

			request.getRequestDispatcher("Signup.jsp").forward(request, response);

		}

		@Override
		protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
			List<String> messages = new ArrayList<String>();


			HttpSession session = request.getSession();
			if (isValid(request, messages) == true) {
				User user = new User();
				user.setLogin_id(request.getParameter("login_id"));
				user.setName(request.getParameter("name"));
				user.setPassword(request.getParameter("password"));
				user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
				user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
				new UserService().register(user);
				response.sendRedirect("Management");

			} else {
				session.setAttribute("errorMessages", messages);
				request.setAttribute("login_id",request.getParameter("login_id"));
				request.setAttribute("name",request.getParameter("name"));
				//request.setAttribute("password",request.getParameter("password"));
				request.setAttribute("branch_id",request.getParameter("branch_id"));
				request.setAttribute("department_id",request.getParameter("department_id"));

				List<Branch> branchName = editService.getBranchName();
				List<Department> departmentName = editService.getDepartmentName();

				request.setAttribute("branchName",branchName);
				request.setAttribute("departmentName",departmentName);


				request.getRequestDispatcher("Signup.jsp").forward(request, response);
			}
		}
		private boolean isValid(HttpServletRequest request, List<String> messages) {
			String Login_id = request.getParameter("login_id");
			String password = request.getParameter("password");
			String password_confirm = request.getParameter("password_confirm");
			String name = request.getParameter("name");
			String branch_id = request.getParameter("branch_id");
			String department_id = request.getParameter("department_id");
			boolean idCheck = new UserService().getLogin_id(Login_id);

		       // 名前のバリデーション
	        if (StringUtils.isBlank(name) == true) {
	            messages.add("・名前を入力してください");
	        } else {
	        	if (name.length() > 10) {
	                messages.add("・名前は10文字以下です");
	            }
	        }
			//ログインのバリデーション
			if (StringUtils.isEmpty(Login_id) == true) {
				messages.add("・ログインIDを入力してください");
			}else{
				if((Login_id.length()<6) || (Login_id.length()>20)) {
					messages.add("・ログインIDは6文字以上、20文字以下で入力してください");
				}
				if(!Login_id.matches("^[0-9a-zA-Z]+$")){
					messages.add("・ログインIDは半角英数字です");
				}
			}
			if(idCheck==false){
				messages.add("・ログインIDが重複しています");
			}
			//パスワードのバリデーション
			if (StringUtils.isEmpty(password) == true) {
				messages.add("・パスワードを入力してください");
			}else{
				if((password.length()<6) || (password.length()>20)) {
					messages.add("・パスワードは6文字以上、20文字以下で入力してください");
				}
				if(!password.matches("^[0-9a-zA-Z]+$")){
					messages.add("・パスワードは半角英数字です");
				}
			}
			 // パスワード確認のバリデーション
	        if (StringUtils.isBlank(password_confirm) == true) {
	            messages.add("・確認用パスワードを入力してください");
	        } else {
	        	if (!password.equals(password_confirm)) {
	        		messages.add("・確認用パスワードが一致しません");
	        	}
	        }
	        // 支店のバリデーション
	        if (StringUtils.isBlank(branch_id) == true) {
	            messages.add("・支店を入力してください");
	        }
	        // 部署のバリデーション
	        if (StringUtils.isBlank(department_id) == true) {
	            messages.add("・部署・役職を入力してください");
	        }
	        if(branch_id.matches("[1]") && department_id.matches("[3]")){
	        	messages.add("・支店名と部署・役職の組み合わせが不正です");
	        }
	        if(!branch_id.matches("[1]") && department_id.matches("[1-2]")){
	        	messages.add("・支店名と部署・役職の組み合わせが不正です");
	        }
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

