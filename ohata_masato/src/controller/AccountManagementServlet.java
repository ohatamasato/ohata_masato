package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.AccountService;
import service.ManagementService;

/**
 * Servlet implementation class AccountManagementServlet
 */
@WebServlet("/AccountManagementServlet")
public class AccountManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
	HttpServletResponse response) throws ServletException, IOException {

	User user=new User();

	user.setIs_deleted(Integer.parseInt(request.getParameter("is_deleted")));
	user.setId(Integer.parseInt(request.getParameter("id")));

	AccountService accountService =new AccountService();
	accountService.parameterChange(user);

	HttpSession session =((HttpServletRequest)request).getSession();
	User loginUser =(User)session.getAttribute("loginUser");

	List<User>management =new ManagementService().getdata();
	request.setAttribute("managements", management);
	request.setAttribute("loginUser",loginUser);

	response.sendRedirect("Management");
	}
}
