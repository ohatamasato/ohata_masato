package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Contribute;
import beans.User;
import service.ContributeService;

@WebServlet(urlPatterns = { "/contribute" })
public class NewContributeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    request.getRequestDispatcher("contribute.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");
            Contribute message = new Contribute ();
            message.setCategory(request.getParameter("category"));
            message.setSubject(request.getParameter("subject"));
            message.setText(request.getParameter("msg"));
            message.setUser_id(user.getId());

            new ContributeService().register(message);
            response.sendRedirect("./");

        } else {
            session.setAttribute("errorMessages", messages);

            Contribute reContribution = new Contribute();
            reContribution.setSubject(request.getParameter("subject"));
            reContribution.setCategory(request.getParameter("category"));
            reContribution.setText(request.getParameter("msg"));

            request.setAttribute("reContribution", reContribution);
            request.getRequestDispatcher("contribute.jsp").forward(request,response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String subject = request.getParameter("subject");
        String msg = request.getParameter("msg");
        String category = request.getParameter("category");

        //テキストえらーメッセージ
        if (StringUtils.isEmpty(msg) == true) {
            messages.add("本文を入力してください");
        }else{
        	if(msg.length() >1000){
        		messages.add("本文は1000文字以下で入力してください");
        	}
        }
        //カテゴリー
        if (StringUtils.isEmpty(category) == true) {
        	messages.add("カテゴリーを入力してください");
        }else{
        	if(category.length() >10){
        		messages.add("カテゴリーは10文字以下で入力してください");
        	}
        }
      //件名
        if (StringUtils.isEmpty(subject) == true) {
        	messages.add("件名を入力してください");
        }else{
        	if(subject.length() >30){
        		messages.add("件名は10文字以下で入力してください");
        	}
        }
        if(messages.size() ==0){
        	return true;
        }else{
        	return false;
        }
    }
}