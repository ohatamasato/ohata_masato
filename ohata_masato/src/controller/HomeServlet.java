package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserComment;
import beans.Usercontribute;
import service.CommentService;
import service.ContributeService;

/*
 * Servlet implementation class HomeServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Date date = new Date();

		// 検索機能
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		String category = request.getParameter("category");
		String fromday = request.getParameter("fromday");
		String untilday = request.getParameter("untilday");
		user.setCategory(category);
		user.setFromday(fromday);
		user.setUntilday(untilday);

		if (StringUtils.isEmpty(fromday) && StringUtils.isEmpty(untilday)) {
			fromday = "2018-06-01";
			untilday = date.toString();
		} else if (!StringUtils.isEmpty(fromday) && !StringUtils.isEmpty(untilday)) {
		} else if (!StringUtils.isEmpty(fromday)) {
			untilday = date.toString();
		} else if (!StringUtils.isEmpty(untilday)) {
			fromday = "2018-06-01";
		}

		request.setAttribute("search", user);

		List<Usercontribute> contribute = new ContributeService().getcontribute(category, fromday, untilday);
		request.setAttribute("contributes", contribute);

		List<UserComment> comments = new CommentService().getcomment();
		request.setAttribute("comment", comments);

		if (!contribute.isEmpty()) {
			request.setAttribute("isShowMessageForm", isShowMessageForm);
			request.getRequestDispatcher("/Home.jsp").forward(request, response);

		} else {
			List<String> narrowMessages = new ArrayList<String>();
			narrowMessages.add("絞り込み条件に一致する投稿がありません");
			request.setAttribute("narrowMessages", narrowMessages);
			request.getRequestDispatcher("/Home.jsp").forward(request, response);

		}
	}
}