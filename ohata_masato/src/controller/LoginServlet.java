package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login"})
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

       request.getRequestDispatcher("/Login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(login_id,password);

        HttpSession session = request.getSession();
        if (user != null && user.getis_deleted()==0) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");

        }else if(!login_id.isEmpty()&& password.isEmpty()) {
        	List<String> loginmessages = new ArrayList<String>();
            loginmessages.add("パスワードを入力して下さい");
            session.setAttribute("loginMessages", loginmessages);
			request.setAttribute("login_id",login_id);
			request.getRequestDispatcher("/Login.jsp").forward(request, response);
        }else {
            List<String> loginmessages = new ArrayList<String>();
            loginmessages.add("ログインに失敗しました。");
            session.setAttribute("loginMessages", loginmessages);
			request.setAttribute("login_id",login_id);

			request.getRequestDispatcher("/Login.jsp").forward(request, response);
        }
    }
}