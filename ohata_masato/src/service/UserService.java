package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

      public boolean getLogin_id(String login_id) {
        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            boolean login_idCheck = userDao.getLogin_id(connection, login_id);

            commit(connection);

            return login_idCheck;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


      public boolean getIdCheck(int id,String login_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userIdDao = new UserDao();

            boolean IdCheck = userIdDao.getIdCheck(connection, id,login_id);

            commit(connection);

            return IdCheck;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public boolean getId(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            boolean Id = userDao.getId(connection, id);

            commit(connection);

            return Id;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
     public void reUpdate(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.reserveUpdate(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
     }
}