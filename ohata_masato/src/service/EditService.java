package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Department;
import beans.User;
import dao.ManagementDao;
import dao.UserDao;
import utils.CipherUtil;

public class EditService {

	public List<Branch> getBranchName() {

        Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao branchDao = new ManagementDao();
            List<Branch> ret = branchDao.getBranch(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public List<Department> getDepartmentName() {

        Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao departmentDao = new ManagementDao();
            List<Department> ret = departmentDao.getDepart(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public User getUserEdit(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret =userDao.getUser(connection, user);

            commit(connection);

            return ret.get(0);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public boolean getUserValid(String valid) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();

            if (userDao.getValid(connection, "id", valid)) {
            	return true;
            } else {
            	return false;
            }

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
	public void edit(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			ManagementDao userDao = new ManagementDao();
			userDao.editbranch(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void reEdit(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			ManagementDao userDao = new ManagementDao();
			userDao.editbranch(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
/*
 		public User getUserid(User userId) {

		Connection connection = null;
		try {
			connection = getConnection();

		    UserDao userIdDao =new UserDao();
            User user = UserDao.getUser(connection, userId);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}*/
}