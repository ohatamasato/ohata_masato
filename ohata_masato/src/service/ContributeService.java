package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Contribute;
import beans.Usercontribute;
import dao.ContributeDao;
import dao.DeleteDao;
import dao.UserContributeDao;

public class ContributeService {
	public void register(Contribute message) {

	Connection connection = null;
		try {
			connection = getConnection();
			ContributeDao messageDao = new ContributeDao();
			messageDao.insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 30;
	public List<Usercontribute> getcontribute(String fromday,String untilday,String category) {
		Connection connection = null;
			try {
				connection = getConnection();
				UserContributeDao messageDao = new UserContributeDao();
				List<Usercontribute> ret = messageDao.getUserContribute(connection, fromday, untilday, category, LIMIT_NUM);
				commit(connection);
				return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	    }
	}
	public void registerDelete(int deletecontribute) {
		Connection connection = null;
		try {
			connection = getConnection();
			DeleteDao deleteDao = new DeleteDao();
			deleteDao.delete(connection, deletecontribute);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}