package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exceptions.SQLRuntimeException;

public class CommentDao {
    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("users_Id");
            sql.append(", Contributions_id");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", update_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // users_id
            sql.append(", ?"); // contributions_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // update_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, comment.getId());
            ps.setInt(2, comment.getContributions_id());
            ps.setString(3, comment.getText());


            ps.toString();
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}