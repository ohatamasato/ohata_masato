package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exceptions.SQLRuntimeException;

public class DepartmentDao {

	public List<User> getDepartmentPosition(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM departments";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int department_id = rs.getInt("department_id");
                String department_name = rs.getString("department_name");

                User department = new User();
                department.setDepartment_id(department_id);
                department.setDepartment_Name(department_name);
                System.out.println(department_id);
                System.out.println(department_name);
                ret.add(department);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
