package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exceptions.SQLRuntimeException;

public class UserCommentDao {

	    public List<UserComment> getUserComment(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append(" users.id ");
	            sql.append(", users.name as user_name ");
	            sql.append(", comments.id as comment_id ");
	            sql.append(", comments.text as text ");
	            sql.append(", comments.contributions_id as contributions_id ");
	            sql.append(", comments.users_id as users_id ");
	            sql.append(", comments.created_date as created_date ");
	            sql.append("FROM comments ");
	            sql.append("INNER JOIN users ");
	            sql.append("ON comments.users_Id = users.id ");
	            sql.append("INNER JOIN contributions ");
	            sql.append("ON comments.contributions_id = contributions.id ");
	            sql.append("ORDER BY created_date ASC " );

	            ps = connection.prepareStatement(sql.toString());
	            ps.toString();

	            ResultSet rs = ps.executeQuery();
	            List<UserComment>ret = toUserCommentList(rs);

	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<UserComment> toUserCommentList(ResultSet rs)
	            throws SQLException {

	        List<UserComment> ret = new ArrayList<UserComment>();
	        try {
	            while (rs.next()) {
	                int id = rs.getInt("comment_id");
	                int users_Id = rs.getInt("users_Id");
	                int Contributions_Id = rs.getInt("contributions_id");
	                String name = rs.getString("user_name");
	                String text = rs.getString("text");
	                Timestamp created_Date = rs.getTimestamp("created_date");

	                UserComment comments = new UserComment();
	                comments.setId(id);
	                comments.setUsers_Id(users_Id);
	                comments.setContributions_Id(Contributions_Id);
	                comments.setName(name);
	                comments.setText(text);
	                comments.setCreated_Date(created_Date);

	                ret.add(comments);
	            }
	            return ret;
	        } finally {
	            close(rs);
	    }
	}
}