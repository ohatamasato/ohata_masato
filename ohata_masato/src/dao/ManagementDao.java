package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Department;
import beans.User;
import exceptions.SQLRuntimeException;

public class ManagementDao {
	public List<User> getManagementList(Connection connection){
		PreparedStatement ps = null;
		 try {
        	//ユーザー関連のデータ全部取得用
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.*,");
            sql.append("branches.branch_id,");
            sql.append("branches.branch_name,");
            sql.append("departments.department_id,");
            sql.append("departments.department_name ");
            sql.append("FROM (users INNER JOIN branches ON users.branch_id = branches.branch_id ) ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.department_id ");

            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

    private List<User> toUserList(ResultSet rs)  throws SQLException {
    List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String branch_Name = rs.getString("branch_name");
                String department_Name = rs.getString("department_name");
                int Is_deleted = rs.getInt("is_deleted");
                Timestamp created_Date = rs.getTimestamp("created_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setBranch_Name(branch_Name);
                user.setDepartment_Name(department_Name);
                user.setIs_deleted(Is_deleted);
                user.setCreated_Date(created_Date);
                ret.add(user);

            }
            return ret;
        } finally {
            close(rs);
        }
	}

    public List<Branch> getBranch(Connection connection){
    PreparedStatement ps =null;
        try{
            String sql="SELECT * FROM branches";

            ps=connection.prepareStatement(sql);

                ResultSet rs =ps.executeQuery();
            List<Branch> ret =toBranchList(rs);

            return ret;
        }catch(SQLException e){
            throw new SQLRuntimeException(e);
        }finally{
            close(ps);
        }
    }


    private List<Branch> toBranchList(ResultSet rs)throws SQLException{
        List<Branch> ret =new ArrayList<Branch>();

        try{
            while(rs.next()){
                int Branch_id = rs.getInt("branch_id");
                String branch_name=rs.getString("branch_name");

                Branch branch =new Branch();
                branch.setBranch_id(Branch_id);
                branch.setBranch_name(branch_name);

                ret.add(branch);
            }
            return ret;
        }finally{
            close(rs);
        }
    }
    public void editbranch(Connection connection,User user){

    PreparedStatement ps =null;
	    try{
	        StringBuilder sql= new StringBuilder();

	        sql.append("UPDATE users SET ");
	        sql.append("login_id = ?");
	        sql.append(", name = ? ");
	        sql.append(", branch_id = ?");
	        if(user.getPassword() != null){
	            sql.append(", password = ?");
	        }
	        sql.append(", department_id = ? ");
	        sql.append("WHERE id = ? ");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setString(1, user.getLogin_id());
	        ps.setString(2, user.getName());
	        ps.setInt(3, user.getBranch_id());
	        if(user.getPassword() !=null){
	            ps.setString(4, user.getPassword());
	            ps.setInt(5, user.getDepartment_id());
	            ps.setInt(6, user.getId());
	        }else{
	            ps.setInt(4, user.getDepartment_id());
	            ps.setInt(5, user.getId());
	        }
	            ps.executeUpdate();
	    }catch(SQLException e){
	        throw new SQLRuntimeException(e);
	    }finally {
	        close(ps);
	    }
    }
     public List<Department> getDepart(Connection connection){
    PreparedStatement ps =null;
        try{
            String sql="SELECT * FROM departments";

            ps=connection.prepareStatement(sql);

            ResultSet rs =ps.executeQuery();
            List<Department> ret =toDepartList(rs);

            return ret;
        }catch(SQLException e){
            throw new SQLRuntimeException(e);
        }finally{
            close(ps);
        }
    }
    private List<Department> toDepartList(ResultSet rs)throws SQLException{
	    List<Department> ret =new ArrayList<Department>();
	    try{
	        while(rs.next()){
	            int department_id=rs.getInt("department_id");
	            String department_name=rs.getString("department_name");

	            Department departmentList =new Department();
	            departmentList.setDepartment_id(department_id);
	            departmentList.setDepartment_name(department_name);

	            ret.add(departmentList);
	        }
	        return ret;
	    }finally{
	        close(rs);
	    }
    }
}

