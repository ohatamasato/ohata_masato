package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exceptions.SQLRuntimeException;

public class BranchDao {

	public List<User> getBranch(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM branches";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int branch_id = rs.getInt("branch_id");
                String branch_name = rs.getString("branch_name");

                User branch = new User();
                branch.setBranch_id(branch_id);
                branch.setName(branch_name);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}