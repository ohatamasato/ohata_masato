package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Contribute;
import exceptions.SQLRuntimeException;

public class ContributeDao {
    public void insert(Connection connection, Contribute message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO contributions ( ");
            sql.append("subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, message.getSubject());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getUser_id());


            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}