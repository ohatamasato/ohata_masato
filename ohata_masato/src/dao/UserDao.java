package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exceptions.SQLRuntimeException;

//ユーザーデータDB格納
public class UserDao {
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users (");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_deleted");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // loginID
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // department
            sql.append(", 0"); // is_deleted
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setLong(4, user.getBranch_id());
            ps.setLong(5, user.getDepartment_id());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
//ログイン確認用
    public User getUser(Connection connection, String login_id,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (login_id = ?)AND password = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
//ユーザーデータのリスト化
    private List<User> toUserList(ResultSet rs) throws SQLException {
        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int branch_id = rs.getInt("branch_id");
                int department_id = rs.getInt("department_id");
                int is_deleted = rs.getInt("is_deleted");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setDepartment_id(department_id);
                user.setIs_deleted(is_deleted);
                user.setCreated_Date(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
//IDのみの獲得
    public List<User> getUser(Connection connection, User user) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, user.getId());

    		ResultSet rs = ps.executeQuery();
    		List<User> ret = toUserList(rs);

    		return ret;

    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
//ユーザーデータ更新用
    public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getDepartment_id());
			ps.setInt(6, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }


  //ユーザーデータ(PW除外の)更新用
    public void reserveUpdate(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch_id());
			ps.setInt(4, user.getDepartment_id());
			ps.setInt(5, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

//バリデーション用構文
    public boolean getValid(Connection connection, String str, String valid) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE " + str + " = " + valid;

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty() == true) {
	            return false;
	        } else {
	            return true;
	        }

	    } catch (SQLException e) {
	    	throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
//Managementで使うDAO
	public void edit(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET login_id = ?, password = ?, name = ?, branch_id = ?, department_id = ? WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
            ps.setInt(6, user.getId());

            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


//アカウント機能停止用DAO
    public void accountChange(Connection connection,User user){

    	PreparedStatement ps=null;
    	try{
    		StringBuilder sql =new StringBuilder();
    		sql.append("UPDATE users SET is_deleted = ? WHERE id = ?");

    		ps = connection.prepareStatement(sql.toString());
    		if(user.getis_deleted() ==0){
    			ps.setInt(1,1);
    			ps.setInt(2, user.getId());
    		}else if(user.getis_deleted() == 1){
    			ps.setInt(1, 0);
    			ps.setInt(2, user.getId());
    		}
    		ps.executeUpdate();
    	}catch(SQLException e){
    		throw new SQLRuntimeException(e);
    	}finally{
    		close(ps);
    	}
    }

    //UserID照合用DAO
    private List<User> toLogin_id(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String login_id = rs.getString("login_id");

                User user = new User();
                user.setLogin_id(login_id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public boolean getLogin_id(Connection connection, String login_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT login_id FROM users WHERE login_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toLogin_id(rs);
            if (userList.isEmpty() == true) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<User> toIdCheck(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	String login_id = rs.getString("login_id");
                int id = rs.getInt("id");

                User user = new User();
                user.setLogin_id(login_id);
                user.setId(id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public boolean getIdCheck(Connection connection, int id, String login_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT id, login_id FROM users WHERE id = ? OR login_id =?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, login_id);


            ResultSet rs = ps.executeQuery();
            List<User> userList = toIdCheck(rs);
            if (userList.size() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<User> toId(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");

                User user = new User();
                user.setId(id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public boolean getId(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT id FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toId(rs);
            if (userList.size() == 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}