package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class Loginfilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		//User loginUser = (User) session.getAttribute("loginUser");

		if (((HttpServletRequest) request).getServletPath().equals("/login") &&
		!((HttpServletRequest) request).getServletPath().matches(".+.css")){
			chain.doFilter(request, response);
			return;
		}else if(session.getAttribute("loginUser") ==null){
			List<String>loginmessages=new ArrayList<String>();
			loginmessages.add("ログインの必要があります");
			session.setAttribute("loginMessages", loginmessages);
			((HttpServletResponse) response).sendRedirect("login");
		}else{
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}