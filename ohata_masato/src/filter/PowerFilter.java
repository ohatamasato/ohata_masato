package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/Management","/Edit","/signup" })
public class PowerFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if (loginUser == null) {
            ((HttpServletResponse) response).sendRedirect("./");
		} else if (loginUser.getDepartment_id() != 1) {
			List<String> powermessages = new ArrayList<String>();
            powermessages.add("不正な操作：権限がありません");
            session.setAttribute("powermessages", powermessages);
            ((HttpServletResponse) response).sendRedirect("./");
		} else {
			chain.doFilter(request, response);
		}
		return;
	}
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}