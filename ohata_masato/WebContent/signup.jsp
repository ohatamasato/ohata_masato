<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	 <link href="./css/signup.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	</head>
	<body>
		<div class="signupcontent">
			新規ユーザー登録画面
			<hr size="3" color="#000080">
			<hr size="1" color="#000080">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
							</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="post">
				<ul>
				<li><label for="name"> 名前 </label> <input name="name" id="name" value ="${name}"/></li>
				（名前は10文字以内で入力してください<br />
				<li><label for="login_id"> ログインID </label><input name="login_id"id="login_id" value="${login_id}" /></li>
				（ログインIDは6文字以上、20文字以内の半角英数字で入力してください)<br />
				<li><label for="password"> パスワード </label><input name="password" type="password"
						id="password"value="${password}" /></li>
				（パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力ください）<br />
				<li><label for="password_confirm">パスワード確認用フォーム</label><input name="password_confirm" type="password"
						id="password_confirm"/><br /></li>
				<li><label for="branch_id">支店名</label>
				 	 <select name="branch_id">
                	<c:forEach items="${branchName}" var="branchName">
            			<option value="${branchName.branch_id}">${branchName.branch_name}</option>
                	</c:forEach>
				</select></li>
				<li><label for="department_id">部署・役職名</label>
				 	<select name="department_id">
                	<c:forEach items="${departmentName}" var="departmentName">
            			<option value="${departmentName.department_id}">${departmentName.department_name}</option>
                	</c:forEach>
				</select></li>
				<li><input type="submit" value="登録" /></li>
				</ul>
				<br>

				<br>
				<a href="Management">管理画面に戻る</a>
			</form>
			<div class="copyright">Copyright(c)masato ohata</div>
		</div>
	</body>
</html>