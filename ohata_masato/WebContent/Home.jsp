<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>連絡掲示板</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
    <h2>業務連絡掲示板</h2>


	<%--  確認ダイアログ--%>
    <script type="text/javascript">
		function disp(param){
		if(window.confirm(param + 'を削除してよろしいですか？')){
			return true;
		}else{
			window.alert('キャンセルしました');
			return false;
		}
	}
	</script>

    <%--  表示機能--%>
	<div class="messages">
		<c:if test="${not empty message}">
			<c:forEach items="${errorMessages}" var="message">
				<c:out value="${message}" />
			</c:forEach>
		</c:if>
		<c:remove var="messages" scope="session"/>
	</div>


 	<%--権限エラーメッセージ --%>
 	<div class="powermessages">
 		<c:if test="${not empty powermessages}">
 		 			<c:forEach items="${powermessages}" var="powermessages">
 				<c:out value="${powermessages}" />
			</c:forEach>
		</c:if>
		<c:remove var="powermessages" scope="session"/>
	</div>

	<%--コメントエラーメッセージ --%>
	<div class="commenterrorMessages">
		<c:if test="${not empty commenterrorMessages}">
			<c:forEach items="${commenterrorMessages}" var="commenterrorMessages">
				<c:out value="${commenterrorMessages}" />
			</c:forEach>
		</c:if>
		<c:remove var="commenterrorMessages" scope="session"/>
	</div>
	<%--絞り込みメッセージ --%>
	<c:if test="${ not empty narrowMessages }">
                <div class="narrowMessages">
                    <ul>
                        <c:forEach items="${narrowMessages}" var="narrowMessages">
                            <li><c:out value="${narrowMessages}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="narrowMessages" scope="session" />
            </c:if>



	<%--  メニュー関連 --%>
	<div class="header-fixed">
		<div class="header-bk">
			<div class="header">
				<div class="font">
					<c:if test="${ empty loginUser }">
						<a href="Login.jsp">ログインする</a>
						<a href="contribute">投稿する</a>
					</c:if>
					<c:if test="${ not empty loginUser }">
						<a href="./">ホーム</a>
						<a href="contribute">投稿する</a>
						<c:if test="${loginUser.department_id == 1}">
							<a href="Management">ユーザー管理画面</a>
						</c:if>
			        	<div class=logout><a href="logout">ログアウト</a></div>
					</c:if>
				</div>
				<c:if test="${ not empty loginUser }">
		    	<div class="profile">
		    	<div class="login_id">LOGINID@<c:out value="${loginUser.login_id}" /></div>
		        <div class="name">ログインユーザー名:<c:out value="${loginUser.name}" /></div>
		        </div>
				</c:if>




			<%--検索機能 --%>
				<div class="search">
				<form action="./">
					<label for="day">投稿日付検索</label>
					<input type="date" id="fromday" name="fromday" value="${search.fromday}">～
					<input type="date" id="untilday" name="untilday" value="${search.untilday}"><br />
					<label for="category">カテゴリー</label><input name="category" id="category" value="${search.category}" /><br />
					<input type="submit" value="絞り込み">
				</form>
					</div>
				</div>
			</div>
		</div>


   <%--  投稿・表示機能 --%>
	<div class="contribute">
		<c:forEach items="${contributes}" var="contributes">
			<hr size="50" color="#ffffff">
         	 <div class="name">
				<span class="subject">件名:<c:out value="${contributes.subject}" /></span><br>
				<span class="name">投稿者:<c:out value="${contributes.name}" /></span><br>
				<span class="category">カテゴリ:<c:out value="${contributes.category}" /></span><br>
          	</div>
			<div class="date">作成日:<fmt:formatDate value="${contributes.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br>
			<div class="text"><pre>本文:<br /><c:out value="${contributes.text}" /></pre></div>
		<%--投稿削除機能 --%>
			<c:if test="${loginUser.id eq contributes.user_Id}">
		 		<form method="post" action="ContributeDeleteServlet">
				<input type="hidden" name="contribute_id" value="${contributes.id}">
				<p><input type="submit" value="投稿削除"onClick="return disp('投稿')"></p>
				</form>
			</c:if>

<%--Comment表示機能 --%>
			<div class="comment">

	         	<c:forEach items="${comment}" var="comments">
					<c:if test="${comments.contributions_id eq contributes.id}">
						<div class="name">名前:<c:out value="${comments.name}" /></div><br>
						<div class="date">コメント日時:<fmt:formatDate value="${comments.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<div class="text" ><pre>本文:<br /><c:out value="${comments.text}" /></pre></div>


<%--コメント削除機能 --%>
					<c:if test="${loginUser.id eq comments.users_Id}">
						<form method="post" action="CommentDeleteServlet">
							<input type="hidden" name="comment_id" value="${comments.id}">
							<p><input type="submit" value="コメントの削除" onClick="return disp('コメント')"></p>
						<hr size="5" color="#000080">
						</form>
					</c:if>
				</c:if>
				</c:forEach>
				</div>
<%--コメント投稿機能 --%>
			<form action="CommentServlet" method="post">
				<pre><textarea name="comment" cols="50" rows="5" class="tweet-box"><c:if test="${contributes.id eq commentview.contributions_id}"><c:out value="${commentview.text}" /></c:if></textarea><br />
				<input type="hidden" name="contributions_id" value="${contributes.id}">
				<input type="submit" value="コメントする"></pre>
			</form>
		</c:forEach>
		<c:remove var="commentview" scope="session"/>
	</div>
		<div class="copyright"> Copyright(c)masato ohata</div>
    </body>
</html>