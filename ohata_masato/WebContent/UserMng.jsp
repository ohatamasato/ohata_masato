<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
	<body>
		<div align="CENTER">
			<%--  確認ダイアログ--%>
		    <script type="text/javascript">
				function check(name,param){
				var flag = confirm( name + 'さんのアカウントを'+ param + 'してよろしいですか？')
					return flag;
				}
			</script>


			<%--投稿エラーメッセージ --%>
			<div class="errorMessages">
				<c:if test="${not empty errorMessages}">
					<c:forEach items="${errorMessages}" var="errorMessages">
					<font color="red"><c:out value="${errorMessages}" /></font>
					</c:forEach>
				</c:if>
				<c:remove var="errorMessages" scope="session"/>
			</div>
			<%--ユーザー管理画面 --%>
			ユーザー管理画面
			<hr size="3" color="#000080">
			<hr size="1" color="#000080">
			<div class="step"><a href="./">ホームに戻る</a></div>
			<br>
			<a href="signup">ユーザ新規登録画面</a>



			<TABLE border="1">
				<TR>
				<TH>ID</TH>
				<TH>ログインID</TH>
				<TH>名前</TH>
				<TH>支店</TH>
				<TH>部署・役職</TH>
				<TH>アカウント作成日時</TH>
				<TH>アカウント状況</TH>
				<TH>ユーザー編集</TH>
				</TR>
				<c:forEach items="${getdata}" var="datas" >
					<TR>
						<TD><c:out value="${datas.id}"></c:out></TD>
						<TD><c:out value="${datas.login_id}"></c:out></TD>
						<TD><c:out value="${datas.name}"></c:out></TD>
						<TD><c:out value="${datas.branch_Name}"></c:out></TD>
						<TD><c:out value="${datas.department_Name}"></c:out></TD>
						<TD><fmt:formatDate value="${datas.created_Date}" pattern="yyyy/MM/dd HH:mm:ss" /></TD>
						<TD>
							<form action="AccountManagementServlet" method="post" onsubmit="return submitchk">
								<input type="hidden" name="is_deleted" value="${datas.is_deleted}">
								<input type="hidden" name="id" value="${datas.id}">
								<c:choose>
									<c:when test="${datas.id == loginUser.id }">
					                	ログイン中
					                </c:when>
									<c:when test="${datas.is_deleted ==0 }">
										<input type="submit" value="停止" onClick="return check('${datas.name}','停止')">
									</c:when>
									<c:when test="${datas.is_deleted ==1}">
										<input type="submit" value="復活" onClick="return check('${datas.name}','復活')">
									</c:when>
								</c:choose>
							</form></TD>
						<TD><a href="Edit?id=${datas.id}">編集</a></TD>
					</TR>
				</c:forEach>
			</TABLE>
		</div>
	</body>
</html>