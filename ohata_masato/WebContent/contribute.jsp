<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>投稿画面</title>
</head>
<body>
	<a href="./">ホーム</a>
	<div align="CENTER">
		<h1>掲示板投稿</h1>

		<div class="messages">
			<c:if test="${not empty errorMessages}">
				<c:forEach items="${errorMessages}" var="messages">
					<li><font color="red"><c:out value="${messages}" /></font></li>
				</c:forEach>
			</c:if>
			<c:remove var="errorMessages" scope="session"/>
		</div>

		<form method ="post" action="contribute">
		<table border="1">
				<tr>
				<th><div align="left"><label for="category">カテゴリー</label>
				<input type="text" name="category" value="${reContribution.category}" id="category"
					size="40" maxlength="10" />(10文字以上は入力できません)</div></th>
				</tr>

				<tr>
				<th><div align="left"><label for="subject">件名</label>
				<input type="text" name="subject" value="${reContribution.subject}" id="subject"
					size="40" maxlength="30" />(30文字以上は入力できません)</div></th>
				</tr>

				<tr><td>本文<br /></td></tr>
				<tr><td><textarea name="msg" rows="10"cols="40">${reContribution.text}</textarea>1000文字以下
				</td></tr>
			</table>
			<br />
			<input type="submit" value="投稿">
		</form>
	</div>
</body>
</html>

